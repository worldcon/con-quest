# WorldQuest or Whatever it's called!

WorldQuest is the gamified job board for the World Science Fiction Convention, specifically CoNZealand 2020. 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Running the Repo Locally 

More instructions soon!

In the project directory, you can run:

### `npm install`

This installs all the node dependencies required for this app to run.

This command needs to be run when the project first gets pulled.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# License

More info on the license to be decided soon. 
