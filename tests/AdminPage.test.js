//
// This file is for running integrations tests on the AdminPage section
//

import { unmountComponentAtNode } from "react-dom";

// Setup and Teardown taken directly from the react docs on testing.

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target.
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNote(container);
    container.remove();
    container = null;
});


