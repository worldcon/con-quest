import React, { Component } from 'react';
import './JobCard.css';

class JobCard extends Component {

    // eslint-disable-next-line
    constructor(props) {
        super(props);
        //TODO: Should we have state instead of props? 
    }

    render() {
        let content = this.props.content;
        return (
            <div className="JobCard">
                <h4 className="item">{content.title}</h4>
                <p className="item">{content.duration}</p>
            </div>
        );
    }
}

export default JobCard;
