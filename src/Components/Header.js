import React, { Component } from 'react';
import './Header.css';
import LogoutPanel from './LogoutPanel.js';

class Header extends Component {
    render() {
        return (
            <header>
                <h1 id="title">Con-Quest</h1>
                <LogoutPanel uname="William" />
            </header>
        );
    }
}

export default Header;
