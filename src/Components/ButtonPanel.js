import React, { Component } from 'react';
import Button from './Button.js';
import './ButtonPanel.css';

class ButtonPanel extends Component {
    render() {
        return (
            <div id="ButtonPanel">
                <Button message="NEW JOB" to='/job+builder' />

                {/* TODO: Change 'to' prop to real reference */} 
                <Button message="ALL JOBS" to='/' />
            </div>
        );
    }

}

export default ButtonPanel;
