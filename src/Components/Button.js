import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Button.css';

class Button extends Component {
    render() {
        return (
            <Link className="button-element noselect" to={this.props.to}>
                {this.props.message}
            </Link>
        );
    }

}

export default Button;