import React, { Component } from 'react';
import './LogoutPanel.css';

class LogoutPanel extends Component {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind();
    }

    handleLogout() {
        // TODO: Just a placeholder. Could refactor to AdminPage or Header. 
        // All this'll do is get rid of cookies and route to login page.
        console.log("Logout button clicked");
    }

    render() {
        return ( 
            <div id="LogoutPanel">
                <span>Logged in as: <span className="uname">{this.props.uname}</span></span>
                <button type="button" onClick={this.handleLogout}>Logout</button> 
            </div>
        )
    }
}

export default LogoutPanel;
