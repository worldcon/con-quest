import React from 'react';
import AdminPage from './AdminPage/AdminPage.js';
import JobBuilder from './JobBuilder/JobBuilder.js';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Currently this code is taken from the 'Dev Ed' 
// youtube channel's video titled React Router Tutorial.

function App() {
    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route path="/job+builder" component={JobBuilder}/> 
                    <Route path="/" component={AdminPage}/> 
                    <Route path="/job/:id" component={AdminPage}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
