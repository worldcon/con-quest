import React, { Component } from 'react';
import Header from '../Components/Header.js';
import Button from '../Components/Button.js';
import JobForm from './JobForm.js';
import './JobBuilder.css';

class JobBuilder extends Component {
    render() {
        return (
            <div id="JobBuilder">
                <Header />
                <div id="content">
                    <JobForm />
                    <Button message="HOME" to='/' />
                </div>
            </div>
        );
    }
}

export default JobBuilder;
