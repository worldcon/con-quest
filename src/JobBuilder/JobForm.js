import React, { Component } from 'react';
import FormDropdown from './FormDropdown.js';
import './JobForm.css';

class JobForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title:"Job Title",
            people: 0,
            area:"Tech",
            room: "Big Room",
            startTime: "10:00",
            endTime: "11:00",
            day: "Monday",
            duration:"Duration",
            walking: 0,
            lifting: 0,
            interaction:0,
            desc:"Description",

            areas: [
                "Executive",
                "Exhibits",
                "Facilitation",
                "Facilities",
                "Finance",
                "IT",
                "Member Services",
                "Operations",
                "Programme",
                "Promotions",
                "Publications",
                "Registration",
                "Tech",
                "Welcome",
                "WSFS"
            ],

            rooms: [
                "Big Room",
                "Small Room",
                "I need list of Rooms"
            ]
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
 
        let job = {
            title:       this.state.title,
            area:        this.state.area,
            desc:        this.state.desc,
            startTime:   this.state.startTime,
            endTime:     this.state.endTime,
            day:         this.state.day,
            duration:    this.state.duration,
            walking:     this.state.walking,
            lifting:     this.state.lifting,
            interaction: this.state.interaction,
        }

        console.log(job);
    }

    render() {
        //TODO: Add style, pizzazz.
        return (
            <form onSubmit={this.handleSubmit}>
                <label> 
                    Job Title:
                    <input 
                        type="text" 
                        name="title" 
                        value={this.state.title}
                        maxLength="40"
                        onChange={this.handleChange} />
                </label>

                <label>
                    Number of People Needed:
                    <input
                        type="number"
                        name="people"
                        value={this.state.people}
                        onChange={this.handleChange}
                        min="1"
                        max="20"
                        />
                </label>

                <label>
                    Area: 
                    <FormDropdown
                        name="area"
                        value={this.state.area}
                        options={this.state.areas}
                        onChange={this.handleChange} />
                </label>

                <label>
                    Room:
                    <FormDropdown
                        name="room"
                        value={this.state.room}
                        options={this.state.rooms}
                        onChange={this.handleChange} />
                </label>

                <label>
                    Day: 
                    <FormDropdown
                        name="day"
                        value={this.state.day}
                        options={["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]}
                        onChange={this.handleChange} />
                </label>
                
                <label>
                    Start Time:
                    <input 
                        type="time"
                        name="startTime"
                        min="00:00"
                        max="23:00"
                        onChange={this.handleChange} />
                </label>
                        
                <label>
                    End Time:
                    <input 
                        type="time"
                        name="endTime"
                        min="00:00"
                        max="23:00"
                        onChange={this.handleChange} />
                </label>
                        
                <label>
                    Duration:
                    <input 
                        type="text"
                        name="duration"
                        value={this.state.duration} 
                        onChange={this.handleChange} />
                </label>

                <label>
                    Walking: 
                    <FormDropdown
                        name="walking"
                        value={this.state.walking}
                        options={[0, 1, 2, 3, 4, 5]}
                        onChange={this.handleChange} />
                </label>

                <label>
                    Lifting: 
                    <FormDropdown
                        name="lifting"
                        value={this.state.lifting}
                        options={[0, 1, 2, 3, 4, 5]}
                        onChange={this.handleChange} />
                </label>

                <label>
                    Social Interaction: 
                    <FormDropdown
                        name="interaction"
                        value={this.state.interaction}
                        options={[0, 1, 2, 3, 4, 5]}
                        onChange={this.handleChange} />
                </label>
                        
                <label>
                    Description:
                    <input 
                        type="text"
                        name="desc"
                        value={this.state.desc} 
                        onChange={this.handleChange} />
                </label>
                        
                <input type="submit" value="Submit" />
            </form>
        );
    }
}

export default JobForm;