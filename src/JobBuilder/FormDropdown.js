import React, { Component } from 'react';
import './FormDropdown.css';

class FormDropdown extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        // Changing the target.name in the synthetic event directly means
        // we don't have to have a name attribute for every option tag.

        event.target.name = this.props.name;
        this.props.onChange(event);
    }

    render() {
        const options = this.props.options.map((option) => 
            <option key={option}>{option}</option>
        );

        return (
            <select value={this.props.value} onChange={this.handleChange}>
                {options}
            </select>
        );
    }
}

export default FormDropdown;
