import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './JobsPanel.css';
import JobCard from '../Components/JobCard.js';

class JobsPanel extends Component {
    render() {
        // Load the job data from the JSON file.
        // In future this data will be coming from AWS DynamoDB
        let jobs = require('./test-jobs.json');

        // TODO: Fix noselect not working issue.
        const jobCards = jobs.map((job) =>
            <Link to={"/job/" + job.id} key={job.id}>
                <JobCard content={job} className="noselect"/>
            </Link>
        );

        return (
            <div id="JobsPanel">
                <div id="head">
                    <h2>My Jobs</h2>
                </div>
                <div id="board">
                    {jobCards}
                </div>
            </div>
        );
    }
}

export default JobsPanel;
