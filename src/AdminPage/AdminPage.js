import React, { Component } from 'react';
import './AdminPage.css';
import Header from '../Components/Header.js';
import JobsPanel from './JobsPanel.js';
import ButtonPanel from '../Components/ButtonPanel.js';

class AdminPage extends Component {
    render() { 
        return (
            <div id="AdminPage">
                
                <Header />

                <div id="content">
                    <div id="jobs-column">
                        <JobsPanel />
                    </div>
                    <div id="buttons-column">
                        <ButtonPanel />
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminPage;
